* `handles_classification`
  * Context: primitive
* `handles_multiclass` 
  * Context: primitive
* `handles_multilabel`
  * Context: primitive
* `handles_regression`
  * Context: primitive
* `is_deterministic`
  * Context: primitive
* `compute_resources`
  * Context: primitive
