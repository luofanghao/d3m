import os
import signal
import subprocess
import time
import unittest

import numpy
import pandas
from pyarrow import plasma

from d3m import container
from d3m.metadata import base as metadata_base


class IntList(container.List[int]):
    pass


class TestPlasma(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.process = subprocess.Popen(['plasma_store', '-m', '1000000', '-s', '/tmp/plasma', '-d', '/dev/shm'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, encoding='utf8', preexec_fn=os.setpgrp)
        time.sleep(5)
        cls.client = plasma.connect('/tmp/plasma', '', 0)

    @classmethod
    def tearDownClass(cls):
        cls.client.disconnect()
        os.killpg(os.getpgid(cls.process.pid), signal.SIGTERM)

    def test_list(self):
        l = IntList([1, 2, 3])

        l.metadata = l.metadata.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': IntList,
            'test': 'foobar',
        })

        object_id = self.client.put(l)
        l_copy = self.client.get(object_id)

        self.assertIsInstance(l_copy, IntList)
        self.assertTrue(hasattr(l_copy, 'metadata'))
        self.assertIs(l_copy.metadata.for_value, l_copy)

        self.assertSequenceEqual(l, l_copy)
        self.assertEqual(l.metadata.to_json(), l_copy.metadata.to_json())
        self.assertEqual(l_copy.metadata.query(()).get('test'), 'foobar')

    def test_ndarray(self):
        for name, dtype, values in (
                ('ints', numpy.int64, [1, 2, 3]),
                ('strings', numpy.dtype('<U1'), ['a', 'b', 'c']),
                ('objects', numpy.object, [{'a': 1}, {'b': 2}, {'c': 3}]),
        ):
            array = container.ndarray(numpy.array(values))
            self.assertEqual(array.dtype, dtype, name)

            array.metadata = array.metadata.update((), {
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': container.ndarray,
                'test': 'foobar',
            })

            object_id = self.client.put(array)
            array_copy = self.client.get(object_id)

            self.assertIsInstance(array_copy, container.ndarray, name)
            self.assertTrue(hasattr(array_copy, 'metadata'), name)
            self.assertIs(array_copy.metadata.for_value, array_copy, name)

            self.assertTrue(numpy.array_equal(array, array_copy), name)
            self.assertEqual(array.metadata.to_json(), array_copy.metadata.to_json(), name)
            self.assertEqual(array_copy.metadata.query(()).get('test'), 'foobar', name)

    def test_matrix(self):
        for name, dtype, values in (
                ('ints', numpy.int64, [[1, 2], [3, 4]]),
                ('strings', numpy.dtype('<U1'), [['a', 'b'], ['c', 'd']]),
                ('objects', numpy.object, [[{'a': 1}, {'b': 2}], [{'c': 3}, {'d': 4}]]),
        ):
            matrix = container.matrix(numpy.array(values))
            self.assertEqual(matrix.dtype, dtype, name)

            matrix.metadata = matrix.metadata.update((), {
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': container.matrix,
                'test': 'foobar',
            })

            object_id = self.client.put(matrix)
            matrix_copy = self.client.get(object_id)

            self.assertIsInstance(matrix_copy, container.matrix, name)
            self.assertTrue(hasattr(matrix_copy, 'metadata'), name)
            self.assertIs(matrix_copy.metadata.for_value, matrix_copy, name)

            self.assertTrue(numpy.array_equal(matrix, matrix_copy), name)
            self.assertEqual(matrix.metadata.to_json(), matrix_copy.metadata.to_json(), name)
            self.assertEqual(matrix_copy.metadata.query(()).get('test'), 'foobar', name)

    def test_dataframe(self):
        for name, values in (
                ('ints', {'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}),
                ('mix', {'A': [1, 2, 3], 'B': ['a', 'b', 'c'], 'C': [{'a':  1}, {'b': 2}, {'c': 3}]}),
        ):
            df = container.DataFrame(pandas.DataFrame(values))

            df.metadata = df.metadata.update((), {
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': container.DataFrame,
                'test': 'foobar',
            })

            object_id = self.client.put(df)
            df_copy = self.client.get(object_id)

            self.assertIsInstance(df_copy, container.DataFrame, name)
            self.assertTrue(hasattr(df_copy, 'metadata'), name)
            self.assertIs(df_copy.metadata.for_value, df_copy, name)

            self.assertTrue(df.equals(df_copy), name)
            self.assertEqual(df.metadata.to_json(), df_copy.metadata.to_json(), name)
            self.assertEqual(df_copy.metadata.query(()).get('test'), 'foobar', name)

    # TODO: Enable once fixed: https://issues.apache.org/jira/browse/ARROW-2273
    @unittest.skip
    def test_sparse_dataframe(self):
        sparse = container.SparseDataFrame(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}))

        sparse.metadata = sparse.metadata.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.SparseDataFrame,
            'test': 'foobar',
        })

        object_id = self.client.put(sparse)
        sparse_copy = self.client.get(object_id)

        self.assertIsInstance(sparse_copy, container.SparseDataFrame)
        self.assertTrue(hasattr(sparse_copy, 'metadata'))
        self.assertIs(sparse_copy.metadata.for_value, sparse_copy)

        self.assertTrue(sparse.equals(sparse_copy))
        self.assertEqual(sparse.metadata.to_json(), sparse_copy.metadata.to_json())
        self.assertEqual(sparse_copy.metadata.query(()).get('test'), 'foobar')

    def test_datasets(self):
        dataset = container.Dataset.load('sklearn://boston')

        dataset.metadata = dataset.metadata.update((), {
            'test': 'foobar',
        })

        object_id = self.client.put(dataset)
        dataset_copy = self.client.get(object_id)

        self.assertIsInstance(dataset_copy, container.Dataset)
        self.assertTrue(hasattr(dataset_copy, 'metadata'))
        self.assertIs(dataset_copy.metadata.for_value, dataset_copy)

        self.assertEqual(len(dataset), len(dataset_copy))
        self.assertEqual(dataset.keys(), dataset_copy.keys())
        for resource_name in dataset.keys():
            self.assertTrue(numpy.array_equal(dataset[resource_name], dataset_copy[resource_name]))
        self.assertEqual(dataset.metadata.to_json(), dataset_copy.metadata.to_json())
        self.assertEqual(dataset_copy.metadata.query(()).get('test'), 'foobar')


if __name__ == '__main__':
    unittest.main()
